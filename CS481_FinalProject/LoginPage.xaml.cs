﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_FinalProject
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        void Handle_loginProcedure(object sender, System.EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(userEntry.Text))
            {
                DisplayAlert("Login", "The username or password is blank", "Try Again");
            }
            else
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Handle_loginProcedure)}");
                Navigation.PushAsync(new MainPage());
            }

        }
    }
}
