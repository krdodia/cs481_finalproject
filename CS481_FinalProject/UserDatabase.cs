﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace CS481_FinalProject
{
    public class UserDatabase
    {
        readonly SQLiteAsyncConnection database;

        public UserDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<UserModel>().Wait();
        }

        public Task<int> SaveItemAsync(UserModel item)
        {

            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }

        }
    }
}
