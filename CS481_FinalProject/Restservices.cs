﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CS481_FinalProject
{
    public class Restservices
    {
           HttpClient _client;

            public Restservices()
            {
                _client = new HttpClient();
            }

        public async Task<WeatherData> GetWeatherData(string query)
            {
            WeatherData getWeather = null;
                try
                {
                    var response = await _client.GetAsync(query);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                    getWeather = JsonConvert.DeserializeObject<WeatherData>(content);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("\t\tERROR {0}", ex.Message);
                }

            return getWeather;
            }
        }

}


// <ListView x:Name="CityList" HasUnevenRows="true">
////            <ListView.ItemTemplate>
//              <DataTemplate>
//                <ViewCell>

//               </ViewCell>
// </DataTemplate>
//</ListView.ItemTemplate>
//</ListView>

// <RelativeLayout>
//            <Image Source = "https://cdn.hipwallpaper.com/i/97/65/9A5vMm.jpg"
//                  RelativeLayout.WidthConstraint="{ConstraintExpression Type=RelativeToParent,Property=Width}"
//                  RelativeLayout.HeightConstraint="{ConstraintExpression Type=RelativeToParent,Property=Height }"/>
//          <Grid RelativeLayout.WidthConstraint="{ConstraintExpression Type=RelativeToParent,Property=Width}"
//                RelativeLayout.HeightConstraint= "{ConstraintExpression Type=RelativeToParent,Property=Height}" >
//          </Grid>
// </RelativeLayout>
//AIzaSyCe-BG5s8D88T71gBY-UqyKVDAWmfZ1cms
// <PackageReference Include = "Xamarin.Forms.GoogleMaps" >
//    < Version > 3.0.4</Version>
//   </PackageReference>
//<PackageReference Include = "Xamarin.Forms.Maps" >
//    < Version > 3.4.0.1008975</Version>
//        </PackageReference>

//[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
//private global::Xamarin.Forms.Entry cityName;

//[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
//private global::Xamarin.Forms.Button getWeather;
// <Entry Placeholder = "Enter City" x:Name="cityName" TextColor="Teal"  />
//[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
//private global::Xamarin.Forms.Button getWeather;
//using Plugin.Permissions;