﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using System.Net.Http;
using Plugin.Connectivity;


namespace CS481_FinalProject
{
    public partial class MainPage : MasterDetailPage//ContentPage
    {
       // Restservices restservices;

        public MainPage()
        {

            InitializeComponent();
            IsPresented = false;

        }
        void OnGetMapButtonClicked(object sender, System.EventArgs e){
            var label = NameLabel.Text;
            var lon = CoordLon.Text;
            var lat = CoordLat.Text;
            double ln = Convert.ToDouble(lon);
            double lt = Convert.ToDouble(lat);

            Navigation.PushAsync(new TabbedPage() { Children = { new PinPage(label, ln, lt) } });

            // var location = (Button)sender;
            //var cityInfo = (WeatherData)location.CommandParameter;
            // Navigation.PushAsync(new MapPage(cityInfo));
            //  var menuItem = (MenuItem)sender;
            //var athlete = (AthleteItem)menuItem.CommandParameter;
            //Navigation.PushAsync(new MoreAthleteInfo(athlete));
        }

        void Handle_ClickedSignIn(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new LoginPage());
        }

        void OnSignInButton(){
            Navigation.PushAsync(new LoginPage());
        }
        void OnEditLocationButton()
        {
            Navigation.PushAsync(new LoginPage());
        }
        async void OnGetWeatherButtonClicked(object sender, System.EventArgs e)
        {
           
            string city = cityName.Text;
            var isConnected = CrossConnectivity.Current.IsConnected;
            bool s1 = string.IsNullOrEmpty(city);


            if (isConnected == false)
            {
                await DisplayAlert("Alert", "Please connect to the Internet Connection!", "OK");
            }

           
            if (!string.IsNullOrWhiteSpace(city))


            if (string.IsNullOrWhiteSpace(city))
            {
                await DisplayAlert("Alert", "Please enter a word you want to search", "OK");
            }
            else


            {

                var client = new HttpClient(); 
                var weatherAddress = "https://api.openweathermap.org/data/2.5/weather" + $"?q={city}" + "&units = imperial" + $"&APPID=91f2faed081b3f5d1a640bcffc933c09";
                var uri = new Uri(weatherAddress);

                WeatherData weatherData = null;
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    weatherData = JsonConvert.DeserializeObject<WeatherData>(jsonContent);
                }
                BindingContext = weatherData;


            }

         //   else
          //  {
            //    await DisplayAlert("Alert", "Please enter a word you want to search", "OK");
            //}



        }

    }
}
